import {Router} from 'express';
import {indexController} from '../controllers/indexController'
class IndexRoutes{
    public router:Router=Router();
    constructor(){
        this.config();
    }
    config():void{
        this.router.get('/:id',indexController.gethistorial);
        this.router.get('/atleta/login',indexController.loginAtleta);
        this.router.get('/entrenador/login',indexController.loginEntrenador);
        this.router.get('/entrenador/listado/:id',indexController.getAtleta);
        this.router.post('/medicion',indexController.agregarMedicion);
        this.router.post('/atleta',indexController.crearAtleta);
        this.router.post('/entrenador',indexController.crearEntrenador);
        this.router.put('/atleta/:id',indexController.updateAtleta)
        this.router.put('/entrenador/:id',indexController.updateEntrenador)
        this.router.delete('/atleta/:id',indexController.deleteAtleta);
        this.router.delete('/atleta/:id',indexController.deleteEntrenador);

        //reportes
        this.router.get('/prom/rc/:id',indexController.getPromRC);
        this.router.get('/prom/oxigeno/:id',indexController.getPromOxigeno);
        this.router.get('/prom/temperatura/:id',indexController.getPromTemperatura);
        this.router.get('/max/rc/:id',indexController.getMaxRC);
        this.router.get('/max/oxigeno/:id',indexController.getMaxOxigeno);
        this.router.get('/max/temperatura/:id',indexController.getMaxTemperatura);
        this.router.get('/min/rc/:id',indexController.getMinRC);
        this.router.get('/min/oxigeno/:id',indexController.getMinOxigeno);
        this.router.get('/min/temperatura/:id',indexController.getMinTemperatura);



    }
}

const indexRoutes=new IndexRoutes();
export default indexRoutes.router;