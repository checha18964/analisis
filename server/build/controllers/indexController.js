"use strict";

const { json } = require("express");
const request = require('request');
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.indexController = void 0;
const database_1 = __importDefault(require("../database"));
class IndexController {

    getUsers(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const usuarios = yield database_1.default.query('SELECT * from Usuario');
            if (usuarios.length > 0) {
                return res.json(usuarios);
            }
            res.status(404).send(false);
            console.log("No hay ningun usuario creado");
        });
    }

    //METODO LOGIN NIÑO
    getLogin(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const us = yield database_1.default.query('SELECT usuario,correo,contrasena,nombres,apellidos,dpi,edad, \'etsito\' status from usuario where correo=\"' + req.body.correo 
                + '\" and contrasena=\"' + req.body.contrasena + '\" or usuario=\"'+req.body.usuario + '\" and contrasena=\"' + req.body.contrasena+'\"');
                console.log(us)
                if (us.length>0) {
                    return res.json(us[0]);
                } else {
                    return res.json({ status: "F" })
                }
            }
            catch (error) {
                return res.json({ status: "F" })
                //res.status(404).send(false);
                console.log(error);
            }
        });
    }
//35.202.194.36



    getUser(req, res) {
        const { id } = req.params;
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield database_1.default.query('SELECT * from Usuario where dpi = ?', [id]);
            if (data.length > 0) {
                return res.json(data);
            }
            res.status(404).send(false);
            console.log("El usuario no existe");
        });
    }

 

    createUser(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield database_1.default.query('INSERT INTO Usuario set ?', [req.body]);
                res.json({ status: "Etsito" })
            //res.status(404).send(false);
                console.log(req.body);
            }
            catch (error) {
                res.status(404).send(false);
            }
        });
    }
    updateUser(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            try {
                yield database_1.default.query('update usuario set ? where dpi= ?', [req.body, id]);
                //res.send("Se actualizo con exito")
                res.json({ status: "Etsito" })
            //res.status(404).send(false);
            }
            catch (error) {
                res.status(404).send(false);
            }
            console.log("se actualizo con exito");
        });
    }
    deleteUser(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            try {
                yield database_1.default.query('delete from usuario where dpi= ?', [id]);
                //res.send("Se elimino con exito")
                res.status(200).send(true);
                console.log("se elimino con exito");
            }
            catch (error) {
                res.status(404).send(false);
            }
        });
    }


    //METODO DEL PROYECTO DE ANALISIS=============================================== SI
    getUsersAnalisis(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const usuarios = yield database_1.default.query('SELECT * from usuario');
            if (usuarios.length > 0) {
                return res.json(usuarios);
            }
            res.status(404).send(false);
            console.log("No hay ningun usuario creado");
        });
    }
    
    //--------------------------------------------------------------------------------
    getAvailability(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const usuarios = yield database_1.default.query('SELECT a.name,a.servicedays,a.bonusdays,a.fine from availability a,detalleavailability da where da.id_movie= '+req.body.idmovie
                                                                +' and da.id_availability=a.id');
                return res.json(usuarios);
            }
            catch (error) {
                res.json({ status: "Etsito" })
                //res.status(404).send(false);
                console.log(error);
            }
        });
    }

    getLenguajePelicula(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const usuarios = yield database_1.default.query('SELECT a.name,a.servicedays,a.bonusdays,a.fine from availability a,detalleavailability da where da.id_movie= '+req.body.idmovie
                                                                +' and da.id_availability=a.id');
                return res.json(usuarios);
            }
            catch (error) {
                res.json({ status: "Etsito" })
                //res.status(404).send(false);
                console.log(error);
            }
        });
    }
//XCARRITO------------------------------------------------------------------------------
getCarrito(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const usuarios = yield database_1.default.query('select m.name,m.image, c.cantidad,c.precio,c.id_movie,c.dpi_usuario from carrito c, movie m '
            +' where c.id_movie=m.id'
            +' and c.dpi_usuario='+req.body.dpi);
            return res.json(usuarios);
        }
        catch (error) {
            res.json({ status: "F" })
            //res.status(404).send(false);
            console.log(error);
        }
    });
}

createCarrito(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            yield database_1.default.query('INSERT INTO carrito set ?', [req.body]);
            res.json({ status: "Etsito" })
            console.log(req.body);
        }
        catch (error) {
            console.log(error);
            res.status(404).send(false);
        }
    });
}
deletecarrito(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        //const { id } = req.params;
        try {
            yield database_1.default.query('delete from carrito where dpi_usuario= '+req.body.dpi+' and id_movie='+req.body.idmovie);
            res.json({ status: "Etsito" })
            console.log("se elimino con exito");
        }
        catch (error) {
            res.json({ status: "F" })
        }
    });
}
// ALQUILER-----------------------------------------------------------------------------------
getAlquiler(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const usuarios = yield database_1.default.query('SELECT * from alquiler where alquilado=1 and dpi_usuario= '+req.body.dpi);
            return res.json(usuarios);
        }
        catch (error) {
            res.json({ status: "F" })
            //res.status(404).send(false);
            console.log(error);
        }
    });
}

createAlquiler(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            yield database_1.default.query('INSERT INTO alquiler set ?', [req.body]);
            res.json({ status: "Etsito" })
            console.log(req.body);
        }
        catch (error) {
            console.log(error);
            res.status(404).send(false);
        }
    });
}

updatelquiler(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            yield database_1.default.query('update alquiler set dpi_usuario='+req.dpi+' where llave= '+req.body.llave);
            res.json({ status: "Etsito" })
            console.log(req.body);
        }
        catch (error) {
            console.log(error);
            res.status(404).send(false);
        }
    });
}
// TRANSACCCION-----------------------------------------------------------------------------------
getTransaccion(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const usuarios = yield database_1.default.query('select u.nombres,u.apellidos,u.correo,t.fecha,m.name pelicula,t.montopagar total '+ 
            ' from usuario u, movie m,transaccion t where '+
            ' t.id_movie=m.id and t.dpi_usuario=u.dpi ');
            return res.json(usuarios);
        }
        catch (error) {
            res.json({ status: "F" })
            //res.status(404).send(false);
            console.log(error);
        }
    });
}

createTransaccion(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            yield database_1.default.query('INSERT INTO transaccion values(md5('+req.body.numerotarjeta+'),\''+req.body.nombretarjeta+'\',\''+
            req.body.fechaexpiracion+'\','+req.body.codigoverificador+','+
            req.body.montopagar+',\''+req.body.moneda+'\',date(now()),'
            +req.body.id_movie+','+req.body.dpi_usuario+')');
            res.json({ status: "Etsito" })
            console.log(req.body);
        }
        catch (error) {
            console.log(error);
            res.status(404).send(false);
        }
    });
}
//-----------------------------------------------------------------------------------
updatePeliculaFalse(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        //const { id } = req.params;
        try {
            yield database_1.default.query('update movie set active=\'false\' where id= '+req.body.id);
            //res.send("Se actualizo con exito")
            res.json({ status: "Etsito" })
        //res.status(404).send(false);
        }
        catch (error) {
            console.log(error);
            res.status(404).send(false);
        }
        console.log("se actualizo con exito");
    });
}
updatePeliculaTrue(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        //const { id } = req.params;
        try {
            yield database_1.default.query('update movie set active=\'true\' where id= '+req.body.id);
            yield database_1.default.query('update alquiler set alquilado=0 where llave= '+req.body.llave);

            //res.send("Se actualizo con exito")
            res.json({ status: "Etsito" })
        //res.status(404).send(false);
        }
        catch (error) {
            console.log(error);
            res.status(404).send(false);
        }
        console.log("se actualizo con exito");
    });
}
//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
getUsersAnalisis(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        const usuarios = yield database_1.default.query('SELECT * from usuario');
        if (usuarios.length > 0) {
            return res.json(usuarios);
        }
        res.status(404).send(false);
        console.log("No hay ningun usuario creado");
    });
}

//METODO LOGIN SI
getUserAnalisisLogin(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const usuarios = yield database_1.default.query('SELECT DPI from usuario where DPI=' + req.body.DPI + ' and contrasena=\"' + req.body.contrasena + '\"');
            return res.json(usuarios);
        }
        catch (error) {
            res.status(404).send(false);
            console.log("fallo login");
        }
    });
}

//METODO DE REGISTRO  SI
createUserAnalisis(req, res){
    return __awaiter(this, void 0, void 0, function* () {
        try {
            yield database_1.default.query('INSERT INTO usuario set ?', [req.body]);
            res.json({ status: "Etsito" })
            console.log(req.body);
        }
        catch (error) {
            res.json({ status: "F" })
        }
    });
}

//AGREGAR PELICULAS  SI
createPeliculaAnalisis(req, res){
    return __awaiter(this, void 0, void 0, function* () {
        try {
            yield database_1.default.query('INSERT INTO movie set ?', [req.body]);
            res.json({ status: "Etsito" })
            //res.status(404).send(false);
        }
        catch (error) {
            res.status(404).send(false);
        }
    });
}

//VER PELICULAS SI
getPeliculas(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        const usuarios = yield database_1.default.query('SELECT * from movie where active=\'true\'');
        if (usuarios.length > 0) {
            return res.json(usuarios);
        }
        res.status(404).send(false);
        console.log("No hay ningun usuario creado");
    });
}


//AGREGAR LENGUAJE SI
createLenguaje(req, res){
    return __awaiter(this, void 0, void 0, function* () {
        try {
            yield database_1.default.query('INSERT INTO languages set ?', [req.body]);
            res.json({ status: "Etsito" })
            //res.status(404).send(false);
        }
        catch (error) {
            res.status(404).send(false);
        }
    });
}



//VER UNA PELICULA
getPelicula(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const usuarios = yield database_1.default.query('SELECT * from movie where active=\'true\' and id =' + req.body.idmovie);
            return res.json(usuarios);
        }
        catch (error) {
            res.status(404).send(false);
            console.log("NO EXISTE PELICULA");
        }
    });
}



//VER LENGUAJE DE PELICULA
getPeliculalenguajes(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const usuarios = yield database_1.default.query('SELECT * from languages where id_movie =' + req.body.idmovie);
            return res.json(usuarios);
        }
        catch (error) {
            res.status(404).send(false);
            console.log("NO EXISTE PELICULA");
        }
    });
}


//VER UN USUARIO
getUser(req, res)  {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const usuarios = yield database_1.default.query('SELECT * from usuario where DPI =' + req.body.DPI);
            return res.json(usuarios);
        }
        catch (error) {
            res.status(404).send(false);
            console.log("NO EXISTE PELICULA");
        }
    });
}
//VER CORREO DE TODOS
getUser1(req, res)  {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const usuarios = yield database_1.default.query('SELECT correo,dpi from usuario');
            return res.json(usuarios);
        }
        catch (error) {
            res.status(404).send(false);
            console.log("NO EXISTE PELICULA");
        }
    });
}

getTransaccionUsuario(req, res)  {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const usuarios = yield database_1.default.query('select u.nombres,u.apellidos,u.correo,t.fecha,m.name pelicula,t.montopagar total,a.llave,m.id idmovie '+ 
            ' from usuario u, movie m,transaccion t,alquiler a where '+
            ' t.id_movie=m.id and t.dpi_usuario=u.dpi and a.id_movie=m.id '+
            ' and u.dpi=' + req.body.dpi);
            return res.json(usuarios);
        }
        catch (error) {
            res.status(404).send(false);
            console.log(error);
        }
    });
}

    //VER DISPONIBILIDAD DE PELICULA
    

    
}
exports.indexController = new IndexController();
